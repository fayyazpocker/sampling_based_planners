#!/usr/bin/env python

from Tkinter import *
import yaml
import sys
import subprocess
import rospy
import rospkg
from geometry_msgs.msg import Point
from std_msgs.msg import Bool


class Param_ui:

    def __init__(self, pkg_path):
        '''
            Creating canvas for the UI
        '''
        self.root = Tk()
        self.entries = list()
        self.root.title("RRT Params")
        self.root.attributes("-topmost", True)
        # self.root.geometry('400')

        self.params_filename = pkg_path + "/config/rrt_params.yaml"
        self.exec_file = pkg_path + "/scripts/RRT/rrt_main.py"

        # Reading params from the params file
        with open(self.params_filename) as file:  
            self.params = yaml.load(file)
        # print params

        # frame = Frame(self.root)
        self.canvas = Canvas(self.root, width = 450, height = 530)
        self.canvas.pack()

        x_start_pos = 60
        y_start_pos = 30

        self.mouse_click = [[None, None], [None, None]]
        self.mouse_click_flag = [False, False]
        
        self.butt = []
        self.new_conf_text = []

        self.add_click_butt("Start Conf",self.select_start,x_start_pos, y_start_pos, self.params["start_conf"])
        self.add_click_butt("Goal Conf",self.select_goal,x_start_pos, y_start_pos*2.2, self.params["goal_conf"])

        
        # self.entries.append(self.add_multiple_entries("Start Conf", x_start_pos, y_start_pos))
        # self.entries.append(self.add_multiple_entries("Goal Conf", x_start_pos, y_start_pos*2.2))
        self.entries.append(self.add_multiple_entries("X Conf Bounds", x_start_pos, y_start_pos*8.2))
        self.entries.append(self.add_multiple_entries("Y Conf Bounds", x_start_pos, y_start_pos*9.4))

        self.entries.append(self.add_entry("Bot_diameter", x_start_pos, y_start_pos*3.4))
        self.entries.append(self.add_entry("Sampling", x_start_pos, y_start_pos*4.6))
        self.entries.append(self.add_entry("Tree Length", x_start_pos, y_start_pos*5.8))
        self.entries.append(self.add_entry("Delta Distance", x_start_pos, y_start_pos*7))

        label = Label(text="Verbose", borderwidth=2, relief="groove", bg="lightblue", height=2, width=12)
        self.canvas.create_window(x_start_pos, y_start_pos*10.6, window=label)

        # Add Checkbox for verbose
        self.verbose = IntVar()
        self.check_button = Checkbutton(variable=self.verbose)
        self.canvas.create_window(x_start_pos*2.5, y_start_pos*10.6, window=self.check_button)

        # Add Button for running python file
        button = Button(text='Generate Params', command=self.run_prm, bg="lightgreen", activebackground="brown", height=3, width=20)
        self.canvas.create_window(200, 380, window=button)

        # Add a label and entry for displaying output from the python file
        label = Label(text="Console", borderwidth=2, relief="groove", bg="green", height=2, width=12)
        self.canvas.create_window(x_start_pos, y_start_pos*15, window=label)

        entry = Entry (self.root) 
        self.canvas.create_window(x_start_pos*4.6, y_start_pos*15, width=300, height=30, window=entry)
        self.entries.append(entry)

        self.load_default_entries()

        self.select_pub = rospy.Publisher("/select_conf",Bool,queue_size=1)
        self.vrep_sub = rospy.Subscriber("/mouse_click", Point, self.vrep_callback, queue_size=1)

    def add_click_butt(self,text_var, callback_func, x_pos, y_pos, conf):

        # if(type == "start"):
        label = Label(text=text_var, borderwidth=2, relief="groove", bg="lightblue", height=2, width=12)
        self.canvas.create_window(x_pos, y_pos, window=label)

        self.butt.append((Button(text="Select", bg="gray", justify=LEFT,
            height=1, width=5, command=callback_func)))
        self.canvas.create_window(x_pos*3, y_pos,window=self.butt[-1])

        new_conf = StringVar()
        new_conf.set("(" + str(conf)[1:-1] + ")")

        label = Label(textvariable=new_conf, borderwidth=2, relief="groove", bg="orange", height=2, width=12, font=("Helvetica", 10, "bold"))
        self.canvas.create_window(x_pos*5, y_pos, window=label)
        self.new_conf_text.append(new_conf)

    def vrep_callback(self,data):
        if(self.mouse_click_flag[0] == False):
            self.mouse_click[0][0] = round(data.x,3)
            self.mouse_click[0][1] = round(data.y,3)
            self.butt[0].configure(bg = "grey")
            self.new_conf_text[0].set("(" + str(self.mouse_click[0])[1:-1] + ")")
            self.mouse_click_flag[0] = True 
        elif(self.mouse_click_flag[1] == False):
            self.mouse_click[1][0] = round(data.x,3)
            self.mouse_click[1][1] = round(data.y,3)
            self.butt[1].configure(bg = "grey")
            self.new_conf_text[1].set("(" + str(self.mouse_click[1])[1:-1] + ")")
            self.mouse_click_flag[1] = True

    def select_start(self):
        self.select_pub.publish(Bool())
        self.butt[0].configure(bg = "lightgreen")
        self.butt[1].configure(bg = "grey")
        self.mouse_click_flag[0] = False
        self.mouse_click_flag[1] = True # One at a time
        pass

    def select_goal(self):
        self.select_pub.publish(Bool())
        self.butt[1].configure(bg = "lightgreen")
        self.butt[0].configure(bg = "grey")
        self.mouse_click_flag[1] = False
        self.mouse_click_flag[0] = True
        pass
        
    
    def add_multiple_entries(self, label_text, x_pos, y_pos):
        '''
            Creates label and entry for multiple entry input
        '''
        label = Label(text=label_text, borderwidth=2, relief="groove", bg="lightblue", height=2, width=12)
        self.canvas.create_window(x_pos, y_pos, window=label)

        entry1 = Entry(self.root)
        entry2 = Entry(self.root) 
        self.canvas.create_window(x_pos*3, y_pos, width=80, height=30, window=entry1)
        self.canvas.create_window(x_pos*5, y_pos, width=80, height=30, window=entry2)

        return [entry1, entry2]


    def add_entry(self,label_text, x_pos, y_pos):
        '''
            Creates label and entry for single entry input
        '''

        label = Label(text=label_text, borderwidth=2, relief="groove", bg="lightblue", height=2, width=12)
        self.canvas.create_window(x_pos, y_pos, window=label)

        entry = Entry (self.root) 
        self.canvas.create_window(x_pos*3, y_pos, width=80, height=30, window=entry)

        return entry
    
    def load_default_entries(self):
        '''
            Loads default entries from the param file
        '''

        # self.entries[0][0].insert(0,self.params["start_conf"][0])
        # self.entries[0][1].insert(0,self.params["start_conf"][1])

        # self.entries[1][0].insert(0,self.params["goal_conf"][0])
        # self.entries[1][1].insert(0,self.params["goal_conf"][1])

        self.entries[0][0].insert(0,self.params["conf_space_x_range"][0])
        self.entries[0][1].insert(0,self.params["conf_space_x_range"][1])

        self.entries[1][0].insert(0,self.params["conf_space_y_range"][0])
        self.entries[1][1].insert(0,self.params["conf_space_y_range"][1])

        self.entries[2].insert(0,self.params["bot_diameter"])
        self.entries[3].insert(0,self.params["sampling"])
        self.entries[4].insert(0,self.params["tree_length"])
        self.entries[5].insert(0,self.params["delta_dist_node"])

        if(self.params["verbose"]):
            self.check_button.select()
        else:
            self.check_button.deselect()
        

    def write_to_param(self):
        '''
            Write to param file the current values of entries
        '''

        self.params["start_conf"][0] = self.mouse_click[0][0]
        self.params["start_conf"][1] = self.mouse_click[0][1]

        # Setting goal_configuration 
        self.params["goal_conf"][0] = self.mouse_click[1][0]
        self.params["goal_conf"][1] = self.mouse_click[1][1]

        # Setting X Axis Bounds
        self.params["conf_space_x_range"][0] = float(self.entries[0][0].get())
        self.params["conf_space_x_range"][1] = float(self.entries[0][1].get())

        # Setting Y Axis Bounds
        self.params["conf_space_y_range"][0] = float(self.entries[1][0].get())
        self.params["conf_space_y_range"][1] = float(self.entries[1][1].get())

        self.params["bot_diameter"] = float(self.entries[2].get())
        self.params["sampling"] = self.entries[3].get()
        self.params["tree_length"] = int(self.entries[4].get())
        self.params["delta_dist_node"] = float(self.entries[5].get())


        if (self.verbose.get()):
            self.params["verbose"] = True
        else:
            self.params["verbose"] = False

        # Writing yaml file
        with open(self.params_filename,'w') as file:  
            yaml.dump(self.params,file)
    
    def run_prm(self):
        '''
            Callback for Generate params button. Write yaml file and runs PRM
        '''
        self.write_to_param()
        # os.system('python main.py')
        output = subprocess.check_output([sys.executable, self.exec_file])
        self.entries[-1].delete(0,END)
        self.entries[-1].insert(0," ")
        self.entries[-1].insert(0,output[:-1])
        # print output
        


if __name__ == '__main__':

    rospy.init_node("rrt_gui")

    rospack = rospkg.RosPack()
    package_path = rospack.get_path('sample_planner')

    ui = Param_ui(package_path)
    ui.root.mainloop()