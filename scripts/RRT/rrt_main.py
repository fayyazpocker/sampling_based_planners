from rrt import *
import csv
from rospkg import RosPack


def read_obst_csv(filename):
    '''
        Reads obstacles.csv and returns obstacle's xy and diameter as two lists
        [[x,y],...] , [diameter,...]
        filename: csv file to read obstacle's x,y and diameter information (All obstacles are assumed circular)
    '''
    obst_xy = list()
    obst_dia = list()
    with open(filename) as csv_:
        csv_reader = csv.reader(csv_, delimiter=',')
        for row in csv_reader:
            if(row[0][0] !="#"): # Comment handling
                new_list = [float(i) for i in row]
                obst_xy.append(new_list[:-1])
                obst_dia.append(new_list[-1])
    return obst_xy, obst_dia


def write_csv(list_,filename):
    '''
        write a list to a given csv file 
        list_ : list of list 
    '''
    with open(filename, 'w') as file: # Write the path to path.csv
        writer = csv.writer(file)
        for index,row in enumerate(list_):
            if(filename[-10:-4] == "/nodes"):
                writer.writerow([index + 1] + row)
            elif(filename[-8:-4] == "path"):
                row = [row_ele+1 for row_ele in row] # Increment each node by one for writing to csv
                writer.writerow(row)
            else:
                writer.writerow(row)


if __name__ == "__main__":

    # Package name
    rp = RosPack()
    pkg_loc = rp.get_path('sample_planner')
    config_loc = pkg_loc + '/config/'
    csv_loc = pkg_loc + '/csv_files/RRT/'
    # Params and Obstacles filename
    params_filename =  config_loc + 'rrt_params.yaml'
    obst_filename =  csv_loc + 'obstacles.csv'

    # Files to be written
    node_filename = csv_loc + 'nodes.csv'
    edge_filename = csv_loc + 'edges.csv'
    path_filename = csv_loc + 'path.csv'

    # Files for visualization and debugging
    nodes_reject_filename = csv_loc + 'reject_nodes.csv'

        # Reading params from the params file
    with open(params_filename) as file:  
        params = load(file)
    
    # Start and Goal Configuration
    start_conf = params["start_conf"]
    goal_conf = params["goal_conf"]

    obst_xy, obst_dia = read_obst_csv(obst_filename)

    rrt = RRT(obst_xy, obst_dia, params)

    if(rrt.check_collision(start_conf)):
        print ("Start Configuration collides with the obstacle")
        exit()
    elif(rrt.check_collision(goal_conf)):
        print ("Goal Configuration collides with the obstacle")
        exit()
    else:
        path_flag, path = rrt.generate_tree(start_conf, goal_conf)

    
    if(path_flag):
        print "Path Computed: ", [path_ele +1 for path_ele in path]
    else:
        print "Path Not computed. Try Increasing the tree Length"
    
    # print path_flag, path
    # Writing correspondng csv's
    write_csv(rrt.nodes, node_filename) # Write nodes.csv
    write_csv(rrt.edges, edge_filename) # Write reject_nodes.csv for visualization in V-REP
    write_csv([path], path_filename)


    if(params["verbose"]):
        write_csv(rrt.reject_nodes, nodes_reject_filename) # Write reject_nodes.csv for visualization in V-REP  
        # write_csv(rrt.reject_edges, edges_reject_filename) # Write reject_nodes.csv for visualization in V-REP
    else:
        # Delete the reject_*.csv files if already present
        if os.path.exists(nodes_reject_filename):
            os.remove(nodes_reject_filename)
        
        if os.path.exists(edges_reject_filename):
            os.remove(edges_reject_filename)
