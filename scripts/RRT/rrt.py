#!/usr/bin/env python3
# import csv
# import numpy as np
import scipy.spatial
from yaml import load
from math import atan2, cos, sin, hypot
from rtree import index

class RTree:
    '''
        Class for NN search using R-TREE
    '''
    def __init__(self):
        self.tree = index.Index()

    def insert_node(self,node_no,node):
        self.tree.insert(node_no, node + node, node) # creating new rectangle within the bounds addressed by node_no
    
    def search(self, input_):
        '''
            Search for nearest nodes and return its index i.e the node_no
        '''
        sample_point = input_[:]
        return next(self.tree.nearest(sample_point))

class KDTree:
    '''
        Class for K-NN search
    '''
    def __init__(self, data):
        self.tree = scipy.spatial.cKDTree(data)
    
    def search(self, input_, k=1):
        '''
            Search for k nearest nodes and return its index and distance
        '''
        return self.tree.query(input_, k=k)

class PathTree:
    '''
        Tree to compute shortest path in a RRT
    '''
    # node_no = 0

    def __init__(self, parent=-1):
        # self.__data = PathTree.node_no
        self.__parent = parent
        
        # PathTree.node_no = PathTree.node_no + 1
    
    def get_parent(self):
        return self.__parent

    # def get_node_no(self):
    #     return self.__data

class RRT:

    def __init__(self,obst_xy, obst_dia, params):
        
        self.obst_kd_tree = KDTree(obst_xy) # Generating a KD Tree for finding nearest obstacles
        self.obst_dia = obst_dia
        self.nodes_r_tree = RTree()

        self.conf_space_x_range = params["conf_space_x_range"] # Bounds for x and y dimension
        self.conf_space_y_range = params["conf_space_y_range"]

        self.sampling_algo = params["sampling"]
        self.max_tree_length = params["tree_length"] # No of nodes required

        self.verbose = params["verbose"]
        self.bot_diameter = params["bot_diameter"]

        self.delta_dist = params["delta_dist_node"]
        self.no_step = int(round(self.delta_dist/self.bot_diameter)) # No of steps to be done incremental to test collision

        self.nodes = list()
        self.reject_nodes = list()
        self.edges = list()

        self.path = []
        # self.reject_edges = list()
    
    def sampling_halton(self,nmbr):
        '''
            Generate a uniformly distributed sample point based on halton sequece
            (van_der_corput in each axis with different base)
            nmbr : input to the halton sequence
            return : [sample_x, sample_y]
        '''
        sample_point = list()
        range_ = list()

        # Measure the range of each axis
        range_.append(float(self.conf_space_x_range[1] - self.conf_space_x_range[0])) # Add limit range of X
        range_.append(float(self.conf_space_y_range[1] - self.conf_space_y_range[0])) # Add limit range of Y
        conf_space = [self.conf_space_x_range, self.conf_space_y_range]
        i = nmbr

        base = [2,3] # Prime numbers as base for van der corput

        for axis, base_ in enumerate(base):
            n_th_number, denom = 0, 1.
            while i > 0:
                i, remainder = divmod(i, base_)
                denom *= base_
                n_th_number += (remainder / denom)    
            n_th_number = n_th_number * range_[axis] + conf_space[axis][0] # Convert number within the range of the axis bounds
            sample_point.append(round(n_th_number,3))
            i = nmbr

        return sample_point

    def search_nearest_node(self,sample_point):
        ind = self.nodes_r_tree.search(sample_point)
        dist = hypot(sample_point[1]-self.nodes[ind][1], sample_point[0]-self.nodes[ind][0])

        return dist, ind

    def check_collision(self,conf):
        '''
        Checking if a configuration is collision with obstacles
        '''
        dist, ind = self.obst_kd_tree.search(conf)
        if(dist <= (self.obst_dia[ind]/2.0 + self.bot_diameter/2.0)):
            return True # Collision
        else:
            return False

    def check_edge_collision(self, no_step, start_conf, goal_conf):

        collision = False
        yaw = atan2(goal_conf[1] - start_conf[1], goal_conf[0] - start_conf[0])

        # Taking next step
        x = start_conf[0]
        y = start_conf[1]

        # loop no of times of steps which includes checking the last successive node as well
        # Check collision of x and y by incrementing by bot diameter in direction of the sample point
        for i in range(no_step):            
            x = x + self.bot_diameter * cos(yaw)
            y = y + self.bot_diameter * sin(yaw)
            dist , ind = self.obst_kd_tree.search([x,y])
            if(dist <= (self.obst_dia[ind]/2.0 + self.bot_diameter/2.0)): # Collision                
                # if(self.verbose):
                #     self.reject_edges.append([ind + 1,node_no , dist])
                collision = True
                break
        
        if(collision):
            return True, [round(x,3),round(y,3)]
        else:
            return False, [round(x,3),round(y,3)] # Return new node

    
    def generate_tree(self, start_conf, goal_conf):

        node_no = 0
        reject_node_no = 0
        inp_halton = 0
        path_found = False
        path = []

        self.nodes_r_tree.insert_node(node_no,start_conf)
        self.nodes.append(start_conf)
        node_no = node_no + 1
        # leaf_node = PathTree(parent = -1) # Root Node
        self.path.append(PathTree(parent=-1)) # Root node


        while node_no < self.max_tree_length:
            if(self.sampling_algo == "halton"):
                sample_point = self.sampling_halton(inp_halton) # Generating a sample point in the configuration space

            if(self.check_collision(sample_point)):                
                if(self.verbose):
                    # reject_node_no = reject_node_no + 1
                    self.reject_nodes.append([reject_node_no] + sample_point)
                    reject_node_no = reject_node_no + 1
                    inp_halton = inp_halton + 1
                continue
            
            # Neglect if sample point is already present in the nodes list
            if sample_point in self.nodes:
                inp_halton = inp_halton + 1
                continue
                
                
            # Find nearest node
            dist, ind = self.search_nearest_node(sample_point)

            # If distance of the new sample is less than the delta distance
            no_step = self.no_step

            if(dist <= self.delta_dist):
                no_step = int(round(dist/self.bot_diameter))
                collision, new_node = self.check_edge_collision(no_step, self.nodes[ind], sample_point)
            else:
                collision, new_node = self.check_edge_collision(no_step, self.nodes[ind], sample_point)

            if(not collision): # If no collision

                self.nodes_r_tree.insert_node(node_no,new_node)
                self.nodes.append(new_node)

                self.path.append(PathTree(ind)) # Adding new node with its parent

                node_no = node_no + 1
                self.edges.append([node_no , ind + 1, no_step*self.bot_diameter]) # Add edge

                # Check if new node is near to goal_node
                eucl_dist = hypot(goal_conf[1] - new_node[1], goal_conf[0] - new_node[0])
                if(eucl_dist <= self.delta_dist):
                    no_step = int(round(eucl_dist/self.bot_diameter))
                    collision, _ = self.check_edge_collision(no_step, self.nodes[ind], goal_conf) # Check collision between new node and goal

                    # If goal node is found
                    if(not collision):
                        self.nodes_r_tree.insert_node(node_no,goal_conf)
                        self.nodes.append(goal_conf)

                        path.append(node_no)
                        parent = node_no - 1
                        while (parent != -1):
                            path.append(parent)
                            parent = self.path[parent].get_parent()

                        path = path[::-1] # Reversing the path list         
                        node_no = node_no + 1
                        self.edges.append([node_no , node_no - 1, no_step*self.bot_diameter]) # Add edge
                        path_found = True
                        break

            inp_halton = inp_halton + 1
        
        return path_found, path
   

