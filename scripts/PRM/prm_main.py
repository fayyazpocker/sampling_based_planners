#!/usr/bin/env python
import prm
import a_star
import csv
from yaml import load
from math import hypot
import os
from rospkg import RosPack


def read_obst_csv(filename):
    '''
        Reads obstacles.csv and returns obstacle's xy and diameter as two lists
        [[x,y],...] , [diameter,...]
        filename: csv file to read obstacle's x,y and diameter information (All obstacles are assumed circular)
    '''
    obst_xy = list()
    obst_dia = list()
    with open(filename) as csv_:
        csv_reader = csv.reader(csv_, delimiter=',')
        for row in csv_reader:
            if(row[0][0] !="#"): # Comment handling
                new_list = [float(i) for i in row]
                obst_xy.append(new_list[:-1])
                obst_dia.append(new_list[-1])
    return obst_xy, obst_dia

  
def write_csv(list_,filename):
    '''
        write a list to a given csv file 
        list_ : list of list 
    '''
    with open(filename, 'w') as file: # Write the path to path.csv
        writer = csv.writer(file)
        for index,row in enumerate(list_):
            writer.writerow(row)
    
def add_start_goal_conf(roadmap, start_conf, goal_conf, verbose):
    '''
        Add edges from the start and goal configuration to the PRM
    '''
    start_node = len(roadmap["nodes"]) + 1
    goal_node = start_node + 1
    roadmap["nodes"].append([start_node, start_conf[0], start_conf[1]])
    roadmap["nodes"].append([goal_node, goal_conf[0], goal_conf[1]])

    conf = start_conf
    node_no = start_node
    
    # Add edges 
    for i in range(2):
        edges, reject_edges = prm_.generate_edge(conf, node_no)
        for edge in edges:
            roadmap["edges"].append([node_no] + edge)
        if(verbose):
            for rej_edge in reject_edges:
                roadmap["reject_edges"].append([node_no] + rej_edge)
        conf = goal_conf
        node_no = goal_node
    
    return start_node, goal_node

def generate_dict_a_star(nodes,edges,goal_conf):
    '''
        Generate a dictionary to be inputed to A* search
        nodes: [[node_x,node_y],..]
        edges: [[node_no, connected_node_no, cost_to_go]]
        goal_conf: [goal_x, goal_y]

        returns nodes_list : {node_number: [heuristic_cost , [ [neighbour_node, cost_to_reach_neighbour], ...] ], ...}
    '''
    nodes_list = {}

    for node_no, node in enumerate(nodes):
        eucl_dist = round(hypot(goal_conf[0]-node[1], goal_conf[1] - node[2]),3)
        node.append(eucl_dist) # Adding heuristic cost to go to node
        nodes_list[node_no+1] = [eucl_dist,[]] # Node number starts from 1 and not 0
    
    for edge in edges:
        nodes_list[edge[0]][1].append([edge[1],edge[2]])
        nodes_list[edge[1]][1].append([edge[0],edge[2]])    

    return nodes_list


if __name__ == "__main__":

    # Package name
    rp = RosPack()
    pkg_loc = rp.get_path('sample_planner')
    config_loc = pkg_loc + '/config/'
    csv_loc = pkg_loc + '/csv_files/PRM/'
    # Params and Obstacles filename
    params_filename =  config_loc + 'prm_params.yaml'
    obst_filename =  csv_loc + 'obstacles.csv'

    # Files to be written
    node_filename = csv_loc + 'nodes.csv'
    edge_filename = csv_loc + 'edges.csv'
    path_filename = csv_loc + 'path.csv'

    # Files for visualization and debugging
    nodes_reject_filename = csv_loc + 'reject_nodes.csv'
    edges_reject_filename = csv_loc + 'reject_edges.csv'

    # Reading params from the params file
    with open(params_filename) as file:  
        params = load(file)
    
    # Start and Goal Configuration

    start_conf = params["start_conf"]
    goal_conf = params["goal_conf"]

    obst_xy, obst_dia = read_obst_csv(obst_filename)

    prm_ = prm.PRM(obst_xy, obst_dia, params) # Create an instance of PRM to make a probabilistic roadmap

    roadmap = prm_.generate_roadmap() # Generate roadmap

    # Check if start and goal configuration is in collision with any obstacle
    if(prm_.check_collision(start_conf)):
        print("Start Configuration collides with the obstacle")
        exit()
    
    elif(prm_.check_collision(goal_conf)):
        print("Goal Configuration collides with the obstacle")
        exit()

    # Add start and goal node to nodes and add edges
    else:
        # Adding edges for start and goal configuration to the roadmap
        start_node, goal_node = add_start_goal_conf(roadmap, start_conf, goal_conf, params["verbose"]) # roadmap's node and edges attribute are changd in the function

        # Generate nodes_list to input for A* search
        nodes_list = generate_dict_a_star(roadmap["nodes"],roadmap["edges"],goal_conf)

        # Compute path using A* search
        path = (a_star.a_star_search(nodes_list, start_node, goal_node))
        if(len(path)):
            print "Path Computed: ", path
        else:
            print ("Path Not computed. Try Increasing the samples")

    # Writing correspondng csv's
    write_csv(roadmap["nodes"], node_filename) # Write nodes.csv
    write_csv(roadmap["edges"], edge_filename) # Write reject_nodes.csv for visualization in V-REP
    write_csv([path], path_filename)

    if(params["verbose"]):
        # pass
        write_csv(roadmap["reject_nodes"], nodes_reject_filename) # Write reject_nodes.csv for visualization in V-REP  
        write_csv(roadmap["reject_edges"], edges_reject_filename) # Write reject_nodes.csv for visualization in V-REP
    else:
        # Delete the reject_*.csv files if already present
        if os.path.exists(nodes_reject_filename):
            os.remove(nodes_reject_filename)
        
        if os.path.exists(edges_reject_filename):
            os.remove(edges_reject_filename)
        




    



