#!/usr/bin/env python 
import csv
import numpy as np
from math import sqrt,pi,cos,sin,atan2, hypot
import scipy.spatial
from random import uniform
from a_star import a_star_search

class KDTree:
    '''
        Class for K-NN search
    '''
    def __init__(self, data):
        self.tree = scipy.spatial.cKDTree(data)
    
    def search(self, input_, k=1):
        '''
            Search for k nearest nodes and return its index and distance
        '''
        return self.tree.query(input_, k=k)

class PRM:

    def __init__(self,obst_xy, obst_dia, params):
        '''
            Initialize obstacke kd tree and the params like axis bounds, no of samples, sampling algorithm, no of k edges
        '''

        self.obst_kd_tree = KDTree(obst_xy) # Generating a KD Tree for finding nearest obstacles
        self.obst_dia = obst_dia
        self.nodes_kd_tree = None

        self.conf_space_x_range = params["conf_space_x_range"] # Bounds for x and y dimension
        self.conf_space_y_range = params["conf_space_y_range"]

        self.sampling_algo = params["sampling"]
        self.no_samples = params["no_samples"] # No of nodes required

        self.k_edges = params["k_edges"] # NO of neighbouring nodes to look for possible edges
        self.max_edge_length = params["max_edge_length"]

        self.verbose = params["verbose"]
        self.bot_diameter = params["bot_diameter"]

        self.edge_checked = {} # [node_no : [edge_checked_node,..]]... This dictionary is used to prevent multiple checking of edge between same nodes

        self.nodes = list()
        self.reject_nodes = list()
        self.edges = list()
        self.reject_edges = list()


    def sampling_halton(self,nmbr):
        '''
            Generate a uniformly distributed sample point based on halton sequece
            (van_der_corput in each axis with different base)
            nmbr : input to the halton sequence
            return : [sample_x, sample_y]
        '''
        sample_point = list()
        range_ = list()

        # Measure the range of each axis
        range_.append(float(self.conf_space_x_range[1] - self.conf_space_x_range[0])) # Add limit range of X
        range_.append(float(self.conf_space_y_range[1] - self.conf_space_y_range[0])) # Add limit range of Y
        conf_space = [self.conf_space_x_range, self.conf_space_y_range]
        i = nmbr

        base = [2,3] # Prime numbers as base for van der corput

        for axis, base_ in enumerate(base):
            n_th_number, denom = 0, 1.
            while i > 0:
                i, remainder = divmod(i, base_)
                denom *= base_
                n_th_number += (remainder / denom)    
            n_th_number = n_th_number * range_[axis] + conf_space[axis][0] # Convert number within the range of the axis bounds
            sample_point.append(round(n_th_number,3))
            i = nmbr

        return sample_point

    
    def generate_nodes(self):
        '''
        Generate collision free nodes and returns both rejected and selected nodes list
        nodes : [[node_x, node_y],[...],...]
        reject_nodes : [[reject_node_x, reject_node_y],[...],...]
        '''
        sample_no = 0
        inp_halton = 0 # Input number to halton sequence 

        while sample_no < self.no_samples:
            if(self.sampling_algo == "halton"):
                sample_point = self.sampling_halton(inp_halton) # Generating a sample point in the configuration space

            dist, ind = self.obst_kd_tree.search(sample_point) # Check nearest obstacle
            if(dist <= (self.obst_dia[ind]/2.0 + self.bot_diameter/2.0)): # Checking collision with the obstacle i.e. if distance is less than the radius of the obstacle
                if(self.verbose):
                    self.reject_nodes.append(sample_point)
            else:
                self.nodes.append(sample_point)
                sample_no = sample_no + 1 # Increment succesfull sample point or node
            
            inp_halton = inp_halton + 1

    def generate_edge(self,node, node_no): # node_no starts with 0 hence less than one compared to one in csv
        '''
            Generate edge for given node and correspondin node number
            edges: [[neigh_node, cost_to_go],...]
            reject_edges: [[reject_neigh_node, cost_to_go], ...] # For visualization
        '''
        edges=list()
        reject_edges = list() 

        start_x = node[0]
        start_y = node[1]
        

        if(node not in self.nodes):
            k_edges = self.k_edges
            node_present = False
        else:
            k_edges = self.k_edges + 1
            node_present = True

        dist, ind = self.nodes_kd_tree.search(node,k=k_edges) # Finding k nearest neighbours

        if(node_present): # Delete 0th element in node is already present as it gives 0
            dist = np.delete(dist,0) # Deleting the node itself
            ind = np.delete(ind,0)

        if(type(ind) is not np.ndarray): # search returns integer if k is only 1
            ind = [ind]
            dist = [dist]

        for index, neigh_node_no in enumerate(ind): 

            if neigh_node_no in self.edge_checked.keys():
                if node_no in self.edge_checked[neigh_node_no]: # Check if collision have already been tested between these edges
                    continue
            
            collision = False
            neigh_node = self.nodes[neigh_node_no]

            goal_x = neigh_node[0]
            goal_y = neigh_node[1]

            yaw = atan2(goal_y - start_y, goal_x - start_x)
            eucl_dis = dist[index] # Eucledian distance between sample node and current goal node
            
            if(eucl_dis >= self.max_edge_length): # Reject if edge lenght exceeds maximum lenght of edge allowed
                if(self.verbose):
                    reject_edges.append([neigh_node_no + 1, round(eucl_dis,3)])
                if(node_no in self.edge_checked.keys()): # The node will not be present while quering outside the library to find edge for start or goal configuration
                    self.edge_checked[node_no].append(neigh_node_no) # Adding the node already tried to avoid retry
                continue
            
            nstep = round(eucl_dis/self.bot_diameter) # No of iterative step for an edge
            x = start_x
            y = start_y

            # Forward simulating by stepsize to find if the edge is in collision with the obstacle or not
            for i in range(int(nstep)):
                dist_, ind_ = self.obst_kd_tree.search([x,y]) # Check for collision with the obstacle
                if(dist_ <= (self.obst_dia[ind_]/2.0 + self.bot_diameter/2.0)): # Collision if the point
                    if(self.verbose):
                        reject_edges.append([neigh_node_no + 1, round(eucl_dis,3)])
                    if(node_no in self.edge_checked.keys()): # The node will not be present while quering outside the library to find edge for start or goal configuration
                        self.edge_checked[node_no].append(neigh_node_no)
                    collision = True
                    break
                else:
                    x = x + self.bot_diameter * cos(yaw) # Go to the next step
                    y = y + self.bot_diameter * sin(yaw)
            
            if(collision == False):
                edges.append([neigh_node_no + 1, round(eucl_dis,3)])
                if(node_no in self.edge_checked.keys()): # The node will not be present while quering outside the library to find edge for start or goal configuration
                    self.edge_checked[node_no].append(neigh_node_no)
        
        return edges, reject_edges     
  

    def generate_all_edges(self):
        '''
            Generate edges for all nearest nodes (as per k_edges)
        '''

        for node_no, node in enumerate(self.nodes):
            self.edge_checked[node_no] = list() # To avoid recomputing edges which is already done
            edges, reject_edges = self.generate_edge(node, node_no) # Generate edge for the node
            for edge in edges:
                self.edges.append([node_no+1] + edge)
            if(self.verbose):
                for rej_edge in reject_edges:
                    self.reject_edges.append([node_no+1] + rej_edge)

    def check_collision(self,conf):
        '''
        Checking if a configuration is collision with obstacles
        '''
        dist, ind = self.obst_kd_tree.search(conf)
        if(dist <= (self.obst_dia[ind]/2.0 + self.bot_diameter/2.0)):
            return True # Collision
        else:
            return False

    
    def generate_roadmap(self):
        '''
            Generate probabilistic roadmap
        '''
        roadmap = {}

        self.generate_nodes() # Generating acceptable nodes list and store in self.nodes and rejected nodes in self.rejects_node
        self.nodes_kd_tree = KDTree(self.nodes) # Generating a KD Tree for finding nearest nodes

        self.generate_all_edges() # Generate edges between nodes

        # roadmap["obst_kd_tree"] = self.obst_kd_tree

        roadmap["nodes"] = [[node_no + 1, node[0], node[1]] for node_no, node in enumerate(self.nodes)]
        # roadmap["nodes_kd_tree"] = self.nodes_kd_tree        

        roadmap["edges"] = self.edges
        

        if self.verbose: # Add rejected nodes and edges if debug is ON
            roadmap["reject_nodes"] = [[node_no + 1, node[0], node[1]] for node_no, node in enumerate(self.reject_nodes)]

            roadmap["reject_edges"] = self.reject_edges
        
        return roadmap