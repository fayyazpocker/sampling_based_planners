#!/usr/bin/env python

class Path_tree():
	"""
	Stores path tree based on A* Graph Search
	"""

	def __init__(self,past_cost,parent):
		self.__past_cost = past_cost
		self.__parent = parent

	def get_parent(self):
		return self.__parent
	
	def update_past_cost(self,past_cost):
		self.__past_cost = past_cost
	
	def get_past_cost(self):
		return self.__past_cost
	
	def update_parent(self,parent):
		self.__parent = parent
	

def a_star_search(nodes_dict,start_node,end_node):
	"""
	Computes shortest path using A* search and returns the path as list:
	[start_node, ... ,goal_node]
    nodes_dict : {node_number: [heuristic_cost , [ [neighbour_node, cost_to_reach_neighbour], ...] ], ...}
	"""

	if(start_node == end_node): # Return if given nodes are same
		return -1

	if((start_node not in nodes_dict.keys()) or (end_node not in nodes_dict.keys())): # Invalid node is given
		return list()

	open_ = dict()
	path_ = dict()
	closed = list()
	path = list()

	open_[start_node] = nodes_dict[start_node][0] # {node_number: total_cost, ...} (For 1st node there is no past cost but only heuristic cost)
	path_[start_node] = Path_tree(0,-1) # {node_number: class object of the node, ...} (-1 indicates no parent)

	while(True):
		if(len(open_.keys()) > 0):
			least_node_val = min(open_.values())
			open_node = list(open_.keys())[list(open_.values()).index(least_node_val)] # Get the node with least total cost

			if(open_node == end_node): # Least cost node reaches the goal node ie Goal is achieved
				curr_node = end_node
				while(curr_node != -1): # Traversing from leaf to the root
					path.insert(0,curr_node)
					curr_node = path_[curr_node].get_parent()
				break
				
			for no_nmbr in range(len(nodes_dict[open_node][1])): # Traverse all the nearest neighbours
					new_node = nodes_dict[open_node][1][no_nmbr][0]
					new_node_cost = nodes_dict[open_node][1][no_nmbr][1]
					new_past_cost = new_node_cost + path_[open_node].get_past_cost() # new cost + past cost already present till that node
					heuristic_cost = nodes_dict[new_node][0]

					if(new_node in closed): # if the node is already closed, do not check again
						continue

					if(new_node not in open_.keys()): # Finding a new node for the first time
						new_root = Path_tree(new_past_cost,open_node)
						open_[new_node] = new_past_cost + heuristic_cost # total cost = heuristic cost + past cost
						path_[new_node] = new_root
					elif(new_past_cost < path_[new_node].get_past_cost()): # Already present hence just need to check if the new past cost is less or not
						open_[new_node] = new_past_cost + heuristic_cost
						path_[new_node].update_parent(open_node)
						path_[new_node].update_past_cost(new_past_cost)
					else:
						continue

			del(open_[open_node]) # Removing node from open list
			closed.append(open_node) # Add the node to the closed list
		else: # No solution is found and hence the open list becomes NULL
			break

	return path
